const REQUIRED_FIELD = 'Обязательно для заполнения'

export const loginValidation = {
	required: REQUIRED_FIELD,
	validate: (value: string) => {
		if (!value.match(/^[a-zA-Z][a-zA-Z0-9-_.]{5,20}$/)) {
			return 'Логин должен содержать 6-20 символов, которыми могут быть буквы и цифры, первый символ обязательно буква'
		}

		return true
	}
}
export const passwordValidation = {
	required: REQUIRED_FIELD,
	validate: (value: string) => {
		const regExp: RegExp =
			/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/

		if (!value.match(regExp)) {
			return 'Пароль должен содержать строчные и прописные латинские буквы, цифры, спецсимволы. Миниум 8 символов'
		}
		return true
	}
}
