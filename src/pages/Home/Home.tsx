import { FC, memo } from 'react'

import EmailForm from '@/components/EmailForm/EmailForm'
import FeaturedProductsCarousel from '@/components/FeaturedProductsCarousel/FeaturedProductsCarousel'
import Footer from '@/components/Footer/Footer'
import GoToUpScroll from '@/components/GoToUpScroll/GoToUpScroll'
import Header from '@/components/Header/Header'
import LatestPostsCarousel from '@/components/LatestPosts/LatestPostsCarousel'
import Main from '@/components/Main/Main'
import PlantBiography from '@/components/PlantBiography/PlantBiography'
import PlantCare from '@/components/PlantCare/PlantCare'
import PlantCollection from '@/components/PlantCollection/PlantCollection'
import { sectionAnimation } from '@/components/PlantCollection/PlantCollection'
import { MPlantCollectionGridItems } from '@/components/PlantCollection/PlantCollectionGridItems/PlantCollectionGridItems'
import { MPlantCollectionMainDescr } from '@/components/PlantCollection/PlantCollectionMainDescr/PlantCollectionMainDescr'
import { MPrePlantBlock } from '@/components/PrePlantCollection/PrePlantBlock'
import UserReviews from '@/components/UserReviews/UserReviews'
import PlantBioStatistic from '@/components/plantBioStatistic/PlantBioStatistic'
import useWindowDimensions from '@/utils/headerResize'

import FavouritesMainIcon from '../Favourites/FavouritesMainIcon'

const Home: FC = () => {
	const { width } = useWindowDimensions()

	return (
		<>
			<Header />
			{width > 1400 && <GoToUpScroll showBelow={1100} />}
			<Main />
			{width > 992 && <MPrePlantBlock />}
			<PlantCollection>
				<MPlantCollectionMainDescr custom={1} variants={sectionAnimation} />
				<MPlantCollectionGridItems />
			</PlantCollection>
			<FavouritesMainIcon />
			<PlantCare />
			<FeaturedProductsCarousel />
			<PlantBiography />
			<PlantBioStatistic />
			<UserReviews />
			<LatestPostsCarousel />
			<EmailForm />
			<Footer />
		</>
	)
}
export default memo(Home)
