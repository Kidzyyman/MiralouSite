import { FC } from 'react'

import { useProducts } from '@/components/hooks/useProducts'
import { IFavourite } from '@/types/IFavourite/IFavourite'
import { IProductsCarouselData } from '@/types/IProductCarousel/IProductsCarouselData'

import FavouriteEmpty from './FavouriteEmpty'
import FavouriteSummary from './FavouriteSummary'

const Favourites: FC<IFavourite | any> = () => {
	const { products } = useProducts()

	const favouriteProductsChoosen = products.filter(
		(product: IProductsCarouselData) => product.choosen
	)

	return (
		<section className='favourites'>
			<div className='container-second'>
				{favouriteProductsChoosen.length !== 0 ? (
					<FavouriteSummary favourite={favouriteProductsChoosen} />
				) : (
					<FavouriteEmpty />
				)}
			</div>
		</section>
	)
}
export default Favourites
