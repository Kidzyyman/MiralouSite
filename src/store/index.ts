import { configureStore } from '@reduxjs/toolkit'

import cartReducer from './cart/cart'
import { getTotals } from './cart/cart'
import favouriteReducer from './favourite/favourite'

export const store = configureStore({
	reducer: {
		cartReducer,
		favouriteReducer
	}
})
export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
export type AppState = ReturnType<typeof store.getState>
store.dispatch(getTotals())
