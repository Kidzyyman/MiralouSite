import React, { forwardRef } from 'react'

import { motion } from 'framer-motion'

import { Props } from '@/types/TProps'

const MainButtons = forwardRef<HTMLDivElement, Props>(({ children }, ref) => {
	return (
		<div ref={ref} className='main-block-btns'>
			{children}
		</div>
	)
})
export default MainButtons

export const MMainButton = motion(MainButtons)
