import { FC, forwardRef } from 'react'

import { motion } from 'framer-motion'

import MainGrass from '@/assets/main-grass.png'

const MainImage: FC = forwardRef<HTMLImageElement>((_, ref) => {
	return (
		<img
			ref={ref}
			src={MainGrass}
			className='main-grass__img'
			alt={MainGrass}
		/>
	)
})
export default MainImage

export const MMainImage = motion(MainImage)
