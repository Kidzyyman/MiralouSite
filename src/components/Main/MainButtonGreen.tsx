import { FC } from 'react'
import { Link } from 'react-router-dom'

const MainButtonGreen: FC = () => {
	return (
		<Link to={'/cart'}>
			<button className='main-btn__green'>
				<span>Add To Cart</span>
			</button>
		</Link>
	)
}
export default MainButtonGreen
