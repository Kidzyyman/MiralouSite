import { FC } from 'react'

import { faHeart } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { IFavouriteIconActive } from '@/types/IFavouriteIconActive/IFavouriteIconActive'

const FavoriteIcon: FC<IFavouriteIconActive> = ({
	addFavouriteItem,
	favouriteItem
}) => {
	return (
		<FontAwesomeIcon
			onClick={() => addFavouriteItem()}
			icon={faHeart}
			className={
				favouriteItem?.choosen ? 'favouritesIcon_active' : 'favouritesIcon'
			}
		/>
	)
}
export default FavoriteIcon
