import { FC } from 'react'

import { Autoplay, Grid, Pagination } from 'swiper'
import 'swiper/css'
import 'swiper/css/grid'
import 'swiper/css/pagination'
import { Swiper, SwiperSlide } from 'swiper/react'

import { IProductsCarouselData } from '@/types/IProductCarousel/IProductsCarouselData'

import { useProducts } from '../hooks/useProducts'
import FeaturedProductsItem from './FeaturedProductsItem'

const FeaturedProductsCarousel: FC = () => {
	const { swiperRef, products, addToCartItems, width } = useProducts()

	return (
		<section className='featuredProductsCarousel-section'>
			<div className='plantCollection-main-descr'>
				<h2 className='plantCollection-main-descr__title'>Featured Products</h2>
				<h2 className='plantCollection-main-descr__subtitle'>
					Smoothlu And healthy plant fore you and all people
				</h2>
			</div>

			<div className='featuredProductsCarousel-main'>
				<div className='container-second'>
					<div
						className='ProductCarousel-carousel'
						onMouseEnter={() => swiperRef.current?.swiper.autoplay.stop()}
						onMouseLeave={() => swiperRef.current?.swiper.autoplay.start()}
					>
						<Swiper
							autoplay={
								width > 998
									? { delay: 4000, disableOnInteraction: false }
									: false
							}
							slidesPerView={4}
							grid={{
								rows: 2
							}}
							breakpoints={{
								1600: {
									slidesPerView: 4
								},
								1100: {
									slidesPerView: 3
								},

								769: {
									slidesPerView: 2
								},
								200: {
									slidesPerView: 1
								}
							}}
							spaceBetween={30}
							pagination={{
								clickable: true
							}}
							modules={[Grid, Pagination, Autoplay]}
							className='featuredProductsCarousel-main__block'
							ref={swiperRef}
						>
							<div className='products-wrapper'>
								{products.map((item: IProductsCarouselData) => (
									<SwiperSlide key={item.id}>
										<FeaturedProductsItem
											item={item}
											addToCart={() => addToCartItems(item)}
										/>
									</SwiperSlide>
								))}
							</div>
						</Swiper>
					</div>
				</div>
			</div>
		</section>
	)
}
export default FeaturedProductsCarousel
