import { FC } from 'react'

import { motion } from 'framer-motion'

import { IPlantTitles } from '@/types/IPlantCollection/IPlantTitles'

import { plantTitles } from './items'

const sectionAnimation = {
	hidden: {
		y: 100,
		opacity: 0
	},
	visible: (custom: number) => ({
		y: 0,
		opacity: 1,
		transition: { delay: custom * 0.1 }
	})
}
const PrePlantBlock: FC = () => {
	return (
		<motion.div
			viewport={{ once: true, amount: 0.2 }}
			initial='hidden'
			whileInView='visible'
			className='preplant-section'
		>
			<div className='container-second'>
				<div className='preplant-block'>
					{plantTitles.map((item: IPlantTitles, i) => (
						<motion.div
							key={i}
							viewport={{ once: true }}
							initial='hidden'
							whileInView='visible'
							custom={i + 1}
							variants={sectionAnimation}
							className='preplant-block__item'
						>
							<img
								src={item.img}
								alt={item.title}
								className='preplant-block__img'
							/>
							<div className='preplant-block-right'>
								<h3 className='preplant-block-right__title'>{item.title}</h3>
								<h4 className='preplant-block-right__subtitle'>
									{item.subtitle}
								</h4>
							</div>
						</motion.div>
					))}
				</div>
			</div>
		</motion.div>
	)
}
export default PrePlantBlock
export const MPrePlantBlock = motion(PrePlantBlock)
