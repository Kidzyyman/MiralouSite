import { FC, ForwardedRef, forwardRef } from 'react'
import { Link } from 'react-router-dom'

import {
	Card,
	CardActions,
	CardContent,
	CardMedia,
	Typography
} from '@mui/material'
import { motion } from 'framer-motion'

import { ILatestPosts } from '@/types/ILatestPosts/ILatestPosts'

const LatestPostsItem: FC<ILatestPosts> = forwardRef(
	({ image, date, description }, ref: ForwardedRef<HTMLDivElement>) => {
		return (
			<div ref={ref} className='latestPost-block'>
				<Card sx={{ maxWidth: 357 }}>
					<CardMedia
						component='img'
						alt='latest post'
						height='200'
						image={image}
					/>
					<div className='latestPost-date'>{date}</div>
					<CardContent>
						<Typography
							variant='body2'
							color='text.secondary'
							width={150}
							marginTop={-5}
						>
							{description}
						</Typography>
					</CardContent>
					<CardActions>
						<Link to={''} className='latestPost-link'>
							Read more
						</Link>
					</CardActions>
				</Card>
			</div>
		)
	}
)

export default LatestPostsItem
export const MLatestPostsItem = motion(LatestPostsItem)
