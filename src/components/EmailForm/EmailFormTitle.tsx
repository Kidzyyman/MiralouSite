import { FC } from 'react'

const EmailFormTitle: FC = () => {
	return (
		<div className='plantCollection-main-descr'>
			<h2 className='plantCollection-main-descr__title'>
				Join To Our Newsletter For More Info
			</h2>
			<h2 className='plantCollection-main-descr__subtitle'>
				Cotrary to popular belief, Lorem Ipsum is not simply random text.
			</h2>
		</div>
	)
}

export default EmailFormTitle
