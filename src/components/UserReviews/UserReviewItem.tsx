import { FC, ForwardedRef, forwardRef } from 'react'

import { motion } from 'framer-motion'

import { IUserReviewsCarousel } from '@/types/IUserReviews/IUserReviews'

const UserReviewsItem: FC<IUserReviewsCarousel> = forwardRef(
	(
		{ id, photo, title, subtitle, description },
		ref: ForwardedRef<HTMLDivElement>
	) => {
		return (
			<div ref={ref} key={id} className='userReviewsCarousel-block'>
				<div className='userReviewsCarousel-block-second'></div>
				<img
					src={photo}
					className='userReviewsCarousel-block__icon'
					alt={title}
				/>
				<h2 className='userReviewsCarousel-block__title'>{title}</h2>
				<h3 className='userReviewsCarousel-block__subtitle'>{subtitle}</h3>
				<h4 className='userReviewsCarousel-block__description'>
					{description}
				</h4>
			</div>
		)
	}
)

export default UserReviewsItem

export const MUserReviewsItem = motion(UserReviewsItem)
