import { FC } from 'react'

const UserReviewsTitle: FC = () => {
	return (
		<div className='plantCollection-main-descr'>
			<h2 className='plantCollection-main-descr__title'>User Reviews</h2>
			<h2 className='plantCollection-main-descr__subtitle'>
				Smoothlu And healthy plant fore you and all people
			</h2>
		</div>
	)
}

export default UserReviewsTitle
