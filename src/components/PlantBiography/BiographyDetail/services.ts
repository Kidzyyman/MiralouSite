import axios, { AxiosResponse } from 'axios'

import { IProductsCarouselData } from '@/types/IProductCarousel/IProductsCarouselData'

export const getBio = (
	id: string | undefined,
	setState: IProductsCarouselData[] | any
) => {
	const data = `https://63c325198bb1ca34755e00d7.mockapi.io/products/${id}`

	axios
		.get(data)
		.then((biography: AxiosResponse<IProductsCarouselData[]>) =>
			setState(biography.data)
		)
}
