import { FC } from 'react'

import { TBurgerMenuProps } from '@/types/THeader/TBurgerMenuProps'

const BurgerMenu: FC<TBurgerMenuProps> = ({
	activeBurgerMenu,
	setActiveBurgerMenu
}) => {
	return (
		<div
			className={activeBurgerMenu ? 'burger-menu active' : 'burger-menu'}
			onClick={() => setActiveBurgerMenu(!activeBurgerMenu)}
		>
			<span></span>
			<span></span>
			<span></span>
		</div>
	)
}
export default BurgerMenu
