import { FC } from 'react'

import { ReactComponent as UserAccountImg } from '../../assets/icons/header-icons/User_icon.svg'

const UserAccount: FC = () => {
	return <UserAccountImg className='user-item' />
}

export default UserAccount
