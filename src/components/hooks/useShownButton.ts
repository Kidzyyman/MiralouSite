import { useCallback, useEffect, useRef, useState } from 'react'

export const useShownButton = (initialSlide: number) => {
	const swiperRef = useRef<any>(null)
	const [currentSlide, setCurrentSlide] = useState(initialSlide)

	const updateIndex = useCallback(
		() => setCurrentSlide(swiperRef.current.swiper.realIndex),
		[]
	)

	useEffect(() => {
		const swiperInstance = swiperRef.current.swiper

		if (swiperInstance) {
			swiperInstance.on('slideChange', updateIndex)
		}

		return () => {
			if (swiperInstance) {
				swiperInstance.off('slideChange', updateIndex)
			}
		}
	}, [updateIndex])

	return {
		currentSlide,
		swiperRef
	}
}
