export const plantGridData = [
	{
		id: 'plant1',
		img: require('../../../assets/plantCollections/pet-friendly-1.png'),
		quantity: 16,
		title: 'Pet Friendly'
	},
	{
		id: 'plant2',
		img: require('../../../assets/plantCollections/pet-friendly-2.png'),
		quantity: 30,
		title: 'Outdoor'
	},
	{
		id: 'plant3',
		img: require('../../../assets/plantCollections/pet-friendly-3.png'),
		quantity: 18,
		title: 'Outdoor'
	},
	{
		id: 'plant4',
		img: require('../../../assets/plantCollections/pet-friendly-4.png'),
		quantity: 29,
		title: 'Pet Friendly'
	},
	{
		id: 'plant5',
		img: require('../../../assets/plantCollections/pet-friendly-5.png'),
		quantity: 25,
		title: 'Indoor'
	},
	{
		id: 'plant6',
		img: require('../../../assets/plantCollections/pet-friendly-6.png'),
		quantity: 17,
		title: 'Indoor'
	},
	{
		id: 'plant7',
		img: require('../../../assets/plantCollections/pet-friendly-7.png'),
		quantity: 21,
		title: 'Office Collection'
	}
]
