import { FC } from 'react'

import { motion } from 'framer-motion'

import { IPlantCollection } from '@/types/IPlantCollection/IPlantCollection'
import { framerMotion } from '@/utils/FramerMotion'

import { MPlantGridItem } from './PlantGridItem/PlantGridBlock'
import { plantGridData } from './plantGridData'

const PlantCollectionGridItems: FC = () => {
	return (
		<motion.section
			viewport={{ once: true }}
			initial='hidden'
			whileInView='visible'
			className='plantCollections-section'
		>
			<div className='container-second'>
				<div className='plantCollections-gridarea'>
					{plantGridData.map((item: IPlantCollection, i) => (
						<MPlantGridItem
							custom={i + 1}
							variants={framerMotion.plantGridDataAnimation}
							key={item.id}
							{...item}
						/>
					))}
				</div>
			</div>
		</motion.section>
	)
}
export default PlantCollectionGridItems
export const MPlantCollectionGridItems = motion(PlantCollectionGridItems)
