class FramerMotion {
	public get plantGridDataAnimation() {
		return {
			hidden: {
				x: -300,
				y: 100,
				opacity: 0
			},
			visible: (custom: number) => ({
				x: 0,
				y: 0,
				opacity: 1,
				transition: { delay: custom * 0.1 }
			})
		}
	}
	public get animationRightToLeft() {
		return {
			hidden: {
				x: 200,
				y: 100,
				opacity: 0
			},
			visible: (custom: number) => ({
				x: 0,
				y: 0,
				opacity: 1,
				transition: { delay: custom * 0.1 }
			})
		}
	}
	public get animationRightUpToLeft() {
		return {
			hidden: {
				x: 200,
				y: -50,
				opacity: 0
			},
			visible: (custom: number) => ({
				x: 0,
				y: 0,
				opacity: 1,
				transition: { delay: custom * 0.1 }
			})
		}
	}
	public get textAnimation() {
		return {
			hidden: {
				x: -100,
				opacity: 0
			},
			visible: (custom: number) => ({
				x: 0,
				opacity: 1,
				transition: { delay: custom * 0.1 }
			})
		}
	}
	public get imageAnimation() {
		return {
			hidden: {
				x: 100,
				opacity: 0
			},
			visible: (custom: number) => ({
				x: 0,
				opacity: 1,
				transition: { delay: custom * 0.1 }
			})
		}
	}
}
export const framerMotion = new FramerMotion()
