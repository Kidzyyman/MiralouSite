export const fixedNumTenth = (num: number): number => {
	return +num.toFixed(2)
}
