import { IProductsCarouselData } from './IProductsCarouselData'

export interface IProductsCarousel {
	item: IProductsCarouselData
	addToCart?: () => void
	addToFavourite?: () => void
}
