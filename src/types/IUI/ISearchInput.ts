export interface ISearchInput {
	value: string
	search: (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => void
}
