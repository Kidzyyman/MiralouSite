import { FC } from 'react'

import AppRoutes from './routes/AppRoutes'
import './styles/style.scss'

const App: FC = () => {
	return (
		<div className='App'>
			<AppRoutes />
		</div>
	)
}

export default App
